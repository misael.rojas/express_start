'use strict';

exports.isAdmin = function(req, res, next){
    console.log( req.userAuth );
    if(req.authUser.role != 'admin') {
        return res.status(200).send({ message: 'Acceso denegado para este metodo'});
    }

    next();
};