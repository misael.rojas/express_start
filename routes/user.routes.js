'use strict';

var express = require('express');
var UserController = require('../controllers/users.controller');
var global = require('../global');

var api = express.Router();

var authMd = require('../middlewares/isAuthenticated.middleware');

var multipart = require('connect-multiparty');
var mp_upload = multipart({ 'uploadDir': global.images.user});


api.get( '/testing',  UserController.testing );
// api.get( '/testing', authMd.isAuthenticated, UserController.testing );
api.post( '/register', UserController.save);
api.post( '/login', UserController.login);
api.get( '/users', UserController.getUsers);
api.get( '/user/:id', authMd.isAuthenticated, UserController.getUser );
api.put( '/user/:id', authMd.isAuthenticated, UserController.update );
api.post( '/user/upload-image/:id', [authMd.isAuthenticated, mp_upload], UserController.uploadImage );
api.get( '/user/get-image/:file', [authMd.isAuthenticated], UserController.getImageFile );

module.exports = api;