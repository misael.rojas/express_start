'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = Schema({
    fn: String,
    ln: String,
    email: String,
    password: String,
    role: String,
    image: String,
    status: Boolean
});


module.exports = mongoose.model('User', UserSchema);