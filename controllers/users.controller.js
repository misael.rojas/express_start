'use strict';
//modulos
var bcrypt = require('bcrypt-nodejs');
var fs = require('fs');
var path = require('path');

//models
var User = require('../models/user.model');

//services
var jwtService = require('../services/jwt.service');
var global = require('../global');

function testing(req, res){
    res.status(200).send({
        message: 'testing the user controller and the action test',
        user: req.authUser
    });
}

function save(req, res){

    var user = new User();

    var params = req.body;

    if(params.fn && params.ln && params.email && params.password) {
        user.fb = params.fn;
        user.ln = params.ln;
        user.email = params.email.toLowerCase();
        user.role = 'basic';
        user.image = null;

        User.findOne({ email: params.email.toLowerCase() }, (err, issetUser) => {
            if(err){
                res.status(500).send({message: 'Error al leer la base de datos'});
            }else{
                if(!issetUser){
                    bcrypt.hash(params.password, null, null, function(err, hash){
                        user.password = hash;
                        user.save( (err, userStored) => {
                            if(err){
                                res.status(500).send({message: 'Error al guardar usuario'});
                            }else{
                                if(!userStored){
                                    res.status(404).send({message: 'El usuario no ha sido registrado'});
                                }else{
                                    res.status(200).send({user: userStored});
                                }
                            }
                        });
                    })
                }else{
                    res.status(200).send({user: 'Usuario no guardado. ya existe el emmail registrado'});
                }
            }
        });
        //if()
    }else{
        res.status(200).send({ message: 'Por favor, envia toda la informacion necesaria' });
    }
}

function login(req, res) {

    var params = req.body;
    var email = params.email.toLowerCase();
    var password = params.password;

    User.findOne({ email: email }, (err, user) => {
        if(err){
            res.status(500).send({message: 'Error al leer usuario'});
        }else{

            if(user){
                bcrypt.compare(password, user.password, function(err, check) {
                    if(check){
                        if(params.gettoken){
                            //deveolver token jwt
                            res.status(200).send({
                                token: jwtService.createToken( user )
                            });
                        }else{
                            res.status(200).send({ user });

                        }

                    }else{
                        res.status(404).send({ message: 'Password incorrecto' });
                    }
                });
            }else{
                res.status(404).send({ message: 'Usuario no encontrado'});
            }
        }
    });
}

function update(req, res) {
    var userId = req.params.id;
    var update = req.body;

    if(userId != req.authUser.sub) {
        return res.status(500).send({message: 'Permiso denegado para actualizar este usuario',});
    }

    User.findByIdAndUpdate (userId, update, { new: true }, (err, userUpdated) => {
        if(err) {
            return res.status(500).send({message: 'error al actualizar usuario',});
        }else{
            if(!userUpdated) {
                return res.status(404).send({message: 'El usuario no pudo ser actualizado',});
            }else{
                return res.status(404).send({message: 'usuario actualizado', user: userUpdated});
            }
        }
    })
}

function uploadImage(req, res) {
    var userId = req.params.id;
    var fileName = 'No uploaded';

    if(userId != req.authUser.sub) {
        return res.status(500).send({message: 'Permiso denegado al actualizar este usuario'});
    }

    if(req.files) {
        var filePath = req.files.image.path;
        var fileSplit = filePath.split('/');
        var fileName = fileSplit[3];

        var extSplit = fileName.split('.');
        var ext = extSplit[1];
        var exts = ['jpg', 'png', 'gif', 'jpeg'];


        if(exts.indexOf(ext) <= -1) {
            fs.unlink(filePath, (err) => {
                if(err) {
                    return res.status(200).send({message: 'Extencion no valida y el archivo no pudo ser eliminado'});
                }else{
                    return res.status(200).send({message: 'extencion no valida'});

                }
            });
        }else{
            User.findByIdAndUpdate (userId, { image: fileName }, { new: true }, (err, userUpdated) => {
                if(err) {
                    return res.status(500).send({message: 'Error al actualizar el usuario'});
                }else{
                    if(!userUpdated) {
                        return res.status(404).send({message: 'El usuario no pudo ser actualizado'});
                    }else{
                        return res.status(404).send({message: 'usuario actualizado', user: userUpdated, image: fileName });
                    }
                }
            })
        }

        /* res.status(200).send({
             file_name: fileName,
             file_path: filePath,
             file_split: fileSplit,
             ext: ext,
         })*/
    }else{
        return res.status(200).send({message: 'El archivo no pudo ser actualizado',});
    }
}

function getImageFile(req, res) {
    var imageFile = req.params.file;
    var pathFile = global.site.user_images_dir + '/' + imageFile;

    console.log(imageFile);

    fs.exists(pathFile, function(exists){
        if(exists){
            res.sendFile(path.resolve(pathFile));
        }else{
            return res.status(404).send({message: 'El archivo no existe',});

        }
    })
}

function getUsers(req, res) {
    User.find({status: 1}).exec((err, users) => {
        if(err){
            res.status(500).send({message: 'Error en la solicitud'});
        }else{
            if(!users.length){
                res.status(404).send({message: 'No hay usuarios'});
            }else{
                res.status(200).send({ users: users });
            }
        }
    });
}

function getUser(req, res) {

    var userId = req.params.id;

    User.findOne({ _id: userId }).exec((err, user) => {
        if(err){
            res.status(500).send({message: 'Error in the request'});
        }else{
            if(!users.length){
                res.status(404).send({message: 'Usuario no encontrado'});
            }else{
                res.status(200).send({ user: user });
            }
        }
    });
}

module.exports = {
    testing,
    save,
    login,
    update,
    uploadImage,
    getImageFile,
    getUsers,
    getUser
};