var config = {
    db: {
        host: '127.0.0.1',
        db: 'restaurant_admin',
        /*user: 'root',
        password: '123456',
        database: 'codejobs',
        debug: true,
        //socket: '/var/run/mysqld/mysqld.sock', // For linux...
        socket: '/Applications/XAMPP/xamppfiles/var/mysql/mysql.sock' //For mac...
        */
    },
    site: {
        url: 'http://localhost:3000',
        title: 'RestaurantAdmin',
        language: 'es',
    },
    images: {
        user: './uploads/users/images',
    },
    secret: 'Clav3-Sup3r-M3g4-S3cr3t4-P4r4-R3st4ur4nt-Admin'
};

module.exports = config;